import java.util.Scanner;

public class FindMax {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int number=Integer.parseInt(sc.next());
        int[] num=new int[number];
        for(int i=0;i<number;i++){
            num[i]=(int)sc.nextInt();
        }

        int max = findMaxSum(num);
        System.out.println(max);
    }

    public static int findMaxSum(int[] arr) {

        if (arr.length == 0) {
            return 0;
        }
        int max = arr[0];
        int sum = 0;
        int len = arr.length;
        if (len <= 0) {
            return 0;
        }
        for (int i = 0; i < len; i++) {
            if (sum <= 0) {
                sum = arr[i];
            } else {
                sum += arr[i];
            }
            if (sum > max) {
                max = sum;
            }
        }

        return max;
    }
}


